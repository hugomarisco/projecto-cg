# projecto-cg

A avaliação tem duas componentes: uma componente escrita individual e uma componente prática realizada em grupo (max. 4 elementos). Cada componente vale 50% da nota final, e tem nota mínima de 9 valores.

O trabalho, cujo enunciado se encontra em anexo, tem as seguintes datas de entrega: 

## Entregas
1: 20 de Março 
2: 10 de Abril 
3: 4 de Maio 
4: 29 de Maio

Em todas as fases é necessário submeter o código, ficheiros de exemplo e o relatório relativo à respectiva fase. Todas as submissões devem ser realizadas através do blackboard. 

Os prazos terminam à meia-noite dos respectivos dias. Os trabalhos podem ser submetidos com atraso, sendo que por cada dia de atraso será atribuída uma penalização de 10% sobre a fase respectiva. Para efeitos de penalização, o fim de semana conta como um único dia. 

Cotação por cada fase: 22.5% (15% trabalho, 7.5% relatório)

Extra: 10% (exemplo: movimentação da câmara, cenas criadas, extensões ao formato do XML, view frustum culling, etc...)

Nota: nas apresentações dos executáveis é obrigatório o uso do modo “Release” (ou equivalente).
