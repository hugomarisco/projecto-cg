#ifndef __POINTS__
#define __POINTS__

#include <iostream>
#include "tinyxml/tinyxml.h"
#include <math.h>

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

typedef struct sPoints {
	float coordinates[3];
	struct sPoints *next = NULL;
} Points;

int readPoints(TiXmlNode *root, Points **res);

#endif