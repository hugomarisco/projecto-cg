#include "rotacao.h"

Rotations *pushRotation(float periodo, float eixoX, float eixoY, float eixoZ, Rotations *rotations){
    Rotations *aux = NULL, *aux2 = rotations;
    
    aux = (Rotations*) malloc (sizeof(Rotations));
    aux->eixoX = eixoX;
    aux->eixoY = eixoY;
    aux->eixoZ = eixoZ;
    if(periodo == 0)
        aux->periodo = 0;
    else {
        aux->periodo = periodo*1000;
    }
    aux->next=NULL;
    
    if(!rotations)
        return aux;
    
    while(rotations->next)
        rotations=rotations->next;
    rotations->next = aux;
    return aux2;
}

Rotations* execRotation(Rotations* rotation,long currentTime){
    if(rotation->periodo != 0)
        glRotatef(360*(currentTime%rotation->periodo)/rotation->periodo, rotation->eixoX, rotation->eixoY, rotation->eixoZ);
    /*else
        glRotatef(rotation->angulo, rotation->x, rotation->y, rotation->z);*/
    
    return rotation->next;
}