#ifndef __ESCALA__
#define __ESCALA__

#include <iostream>
#include <GL/glut.h>

typedef struct sScale {
    float x;
    float y;
    float z;
    struct sScale *next;
}*Scale, NScale;

Scale pushScale(float x, float y, float z, Scale scales);
Scale execScale(Scale scale);

#endif