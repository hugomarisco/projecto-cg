#include "points.h"

int readPoints(TiXmlNode *root, Points **res) {
    TiXmlNode *child;
    TiXmlAttribute * attr;
    const char* tag;
    int point_count = 0;
    Points *aux = NULL, *points = NULL, *last = NULL;
    float x = 0.0, y = 0.0, z = 0.0;
    
    for (child = root->FirstChild(); child; child=child->NextSibling()) {
        tag=child->Value();
        if (strcmp(tag, "ponto") == 0) {
            for(attr=child->ToElement()->FirstAttribute(); attr; attr=attr->Next()) {
                if (strcmp(attr->Name(), "x") == 0)
                    x = atof(attr->Value());
                else if (strcmp(attr->Name(), "y") == 0)
                        y = atof(attr->Value());
                    else if (strcmp(attr->Name(), "z") == 0)
                            z=atof(attr->Value());
            }
            aux = (Points *) malloc (sizeof(Points));
            aux->coordinates[0] = x;
            aux->coordinates[1] = y;
            aux->coordinates[2] = z;
            
            if(last != NULL)
                last->next = aux;
            last = aux;
            
            if(points == NULL)
                points = last;

            point_count++;
        }
    }

    *res = points;

    return point_count;
}