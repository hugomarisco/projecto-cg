#include "vbo.h"

Models addVBO(const char *filename, GLuint *buffers, unsigned short *index, int indexes, Models modelList) {

    Vbo vbo = (Vbo) malloc (sizeof(NVbo));    

    vbo->filename = filename;
    vbo->buffers = buffers;
    vbo->index = index;
    vbo->indexes = indexes;

    Models aux = (Models) malloc (sizeof(NModels));
    aux->next = modelList;
    aux->vbo = vbo;
    
    return aux;
}

void drawVBO(Vbo vbo) {
    glBindBuffer(GL_ARRAY_BUFFER,vbo->buffers[0]);
    glVertexPointer(3,GL_FLOAT,0,0);

    glBindBuffer(GL_ARRAY_BUFFER,vbo->buffers[1]);
    glNormalPointer(GL_FLOAT,0,0);
    
    glDrawElements(GL_TRIANGLES, vbo->indexes, GL_UNSIGNED_SHORT, vbo->index);
}

Vbo searchVBO(const char *filename, Models modelList) {
    
    while (modelList) {
        if (strcmp(filename, modelList->vbo->filename) == 0)
            return modelList->vbo;
        modelList = modelList->next;
    }
    return NULL;
}

Models readVBO(const char *filename, Models modelList) {

    int points, indexes, i = 0;
    float *vertex, *normal, fx, fy, fz;
    unsigned short *index, sx, sy, sz;
    GLuint *buffers = NULL;
    
    FILE *f = fopen(filename, "r");
    if(f) {
        fscanf(f, "%d\n", &points);
        vertex = (float *) malloc (points * sizeof(float));
        normal = (float *) malloc (points * sizeof(float));
        
        while (i < points) {
            fscanf(f, "%f %f %f\n", &fx, &fy, &fz);
            vertex[i++] = fx;
            vertex[i++] = fy;
            vertex[i++] = fz;
        }
        
        i = 0;

        fscanf(f, "%d\n", &indexes);
        index = (unsigned short*) malloc (indexes * sizeof(unsigned short));
        
        while (i < indexes) {
            fscanf(f, "%hu %hu %hu\n", &sx, &sy, &sz);
            index[i++] = sx;
            index[i++] = sy;
            index[i++] = sz;
        }
        

        if (fscanf(f, "%f %f %f\n", &fx, &fy, &fz)!=EOF) {
            i=0;
            normal[i++]=fx;
            normal[i++]=fy;
            normal[i++]=fz;
            while (i<points) {
                fscanf(f, "%f %f %f\n", &fx, &fy, &fz);
                normal[i++]=fx;
                normal[i++]=fy;
                normal[i++]=fz;
            }
        }

        fclose(f);
        

        
        //Aloca memória para os buffers
        buffers = (GLuint*) malloc (2 * sizeof(GLuint));
        
        //Guarda pontos na memória da gráfica
        glGenBuffers(2, buffers);
        glBindBuffer(GL_ARRAY_BUFFER, buffers[0]);
        glBufferData(GL_ARRAY_BUFFER, points * sizeof(float), vertex, GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, buffers[1]);
        glBufferData(GL_ARRAY_BUFFER, points * sizeof(float), normal, GL_STATIC_DRAW);


        free(vertex);
        free(normal);

        return addVBO(filename, buffers, index, indexes, modelList);
    }else
        printf("ERRO! Não fez load do ficheiro '%s'!\n",filename);
    
    return modelList;
    
}