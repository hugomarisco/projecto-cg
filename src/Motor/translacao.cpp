#include "translacao.h"

Points *globalPoints = NULL;
int global_point_count = 0;

Translations* pushTranslation(Points *points, Translations *translations, int nPoints, float period, float x, float y, float z) {
    Translations *aux = NULL;
    Translations *toEnd = NULL;
    aux = (Translations *) malloc (sizeof(Translations));
    aux->points = points;
    aux->point_count = nPoints;
    aux->period = period;
    aux->a = 0;
    aux->lastTime = 0;
    aux->pX = x;
    aux->pY = y;
    aux->pZ = z;
    aux->next = NULL;
    if(translations == NULL){
        translations = aux;
    } else {
        if(translations != NULL)
            for(toEnd = translations; toEnd->next != NULL; toEnd=toEnd->next);
        toEnd->next=aux;
    }

    return translations;
}

float getAt(int index, Points *points, int k) {
    int i = 0;
    float res = 0.0;
    Points *aux = points;
    while(i++<index && aux!=NULL){
        aux = aux->next;
    }
    
    res = aux->coordinates[k];
    
    return res;
}

void getCatmullRomPoint(float t, int *indices, float *res, Points *points) {
    
    int i,j,k;
    float aux[4];
    float tt[4];
    tt[0]=t*t*t;
    tt[1]=t*t;
    tt[2]=t;
    tt[3]=1;
	// catmull-rom matrix
	float m[4][4] = {
		{-0.5f,  1.5f, -1.5f,  0.5f},
        { 1.0f, -2.5f,  2.0f, -0.5f},
        {-0.5f,  0.0f,  0.5f,  0.0f},
        { 0.0f,  1.0f,  0.0f,  0.0f}
    	};
    
	res[0] = 0.0; res[1] = 0.0; res[2] = 0.0;
    // Calcular o ponto res = T * M * P
    // sendo Pi = p[indices[i]]
    for (k=0; k<3; k++){
        for (i=0; i<4; i++){
            aux[i] = 0;
            for (j=0; j<4; j++){
                aux[i] += (tt[j]*m[j][i]);
            }
        }
        for(i=0; i<4; i++){
            res[k] += aux[i]*getAt(indices[i],points,k);
        }
    }
}

void getGlobalCatmullRomPoint(float gt, float *res) {
    
	float t = gt * global_point_count; // this is the real global t
	int index = floor(t);  // which segment
	t = t - index; // where within  the segment
    
	// indices store the points
	int indices[4];
	indices[0] = (index + global_point_count-1)%global_point_count;
    indices[1] = (indices[0]+1)%global_point_count;
	indices[2] = (indices[1]+1)%global_point_count;
    indices[3] = (indices[2]+1)%global_point_count;
    
	getCatmullRomPoint(t, indices, res, globalPoints);
}

void renderCatmullRomCurve() {
    int i;
    float res[3];
    glBegin(GL_LINE_LOOP);
    for(i=0; i<2000; i++){
        getGlobalCatmullRomPoint(i/2000.0f,res);
        glVertex3fv(res);
    }
    glEnd();
}

Translations* execTranslation(Translations* translation, long currentTime) {
    if (translation->period != 0){
        global_point_count = translation->point_count;
        globalPoints = translation->points;

        if(translation->lastTime == 0)
            translation->lastTime = currentTime;
        
        else{
            translation->a += ((currentTime-translation->lastTime)/(translation->period*1000));
            translation->lastTime = currentTime;
            
            getGlobalCatmullRomPoint(translation->a,translation->res);
            renderCatmullRomCurve();

            glTranslatef(translation->res[0],translation->res[1],translation->res[2]);
        }
    }
    else {
        glTranslatef(translation->pX,translation->pY,translation->pZ);
    }
    return translation->next;
}