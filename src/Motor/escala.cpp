#include "escala.h"

Scale pushScale(float x, float y, float z, Scale scales) {
    Scale aux = NULL, aux2 = scales;
    
    aux = (Scale) malloc (sizeof(NScale));
    aux->x = x;
    aux->y = y;
    aux->z = z;
    aux->next = NULL;
    
    if(!scales)
        return aux;
    while (scales->next)
        scales = scales->next;
    scales->next = aux;
    
    return aux2;
}


Scale execScale(Scale scale){
    
    glScalef(scale->x, scale->y, scale->z);
    return scale->next;
    
}