#ifndef __VBO__
#define __VBO__

#include <GL/glew.h>

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>


typedef struct sVbo {
    const char *filename;
    GLuint *buffers;
    unsigned short *index;
    int indexes;
    
} *Vbo, NVbo;

typedef struct sModels {    
    Vbo vbo;
    struct sModels *next;
    
} *Models, NModels;


void drawVBO(Vbo vbo);
Models addVBO(const char *filename, GLuint *buffers, unsigned short *index, int indexes, Models modelList);
Models readVBO(const char* filename, Models modelList);
Vbo searchVBO(const char* filename, Models modelList);

#endif