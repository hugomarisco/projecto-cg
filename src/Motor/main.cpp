#include "motor.h"
#include "tinyxml/tinyxml.h"
#include "camera_explorador.h"
#include "camera_FPS.h"
#include "luz.h"

#include <math.h>
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

int tipo_camera = 0;
TiXmlNode *scene = NULL;

long currentTime = 0.0;
Scale scales = NULL, currentScale = NULL;
Rotations *rotations = NULL, *currentRotation = NULL;
Translations *translations = NULL, *currentTranslation = NULL;

void changeSize(int w, int h) {

	// Prevent a divide by zero, when window is too short
	// (you cant make a window with zero width).
	if (h == 0)
		h = 1;

	// compute window's aspect ratio
	float ratio = w * 1.0 / h;

	// Set the projection matrix as current
	glMatrixMode(GL_PROJECTION);
	// Load Identity Matrix
	glLoadIdentity();

	// Set the viewport to be the entire window
    glViewport(0, 0, w, h);

	// Set perspective
	gluPerspective(45.0f, ratio, 1.0f, 1000.0f);

	// return to the model view matrix mode
	glMatrixMode(GL_MODELVIEW);
}

void renderScene(void) {

	// clear buffers
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// set the camera
	glLoadIdentity();

	if (tipo_camera == 1) { modo_explorador(); }
	else if (tipo_camera == 2) { modo_fps(); }
		else {
			gluLookAt(0,0,5,
				0.0, 0.0, 0.0,
				0.0f, 1.0f, 0.0f);
			}

			
	currentTime = glutGet(GLUT_ELAPSED_TIME);
	currentRotation = rotations;
	currentScale = scales;
	currentTranslation = translations;


	setLights();
	drawXMLModels(scene);

	// End of frame
	glutSwapBuffers();
}

void front_menu(int op) {
    switch (op) {
        case 1:
            glPolygonMode(GL_FRONT_AND_BACK,GL_POINT);
            break;
        case 2:
            glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
            break;
        case 3:
            glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
            break;
        case 4:
            glutKeyboardFunc(teclado_normal_explorador);
            glutSpecialFunc(teclado_especial_explorador);
            glutMouseFunc(rato_explorador);
            glutMotionFunc(mov_rato_explorador);
            tipo_camera=1;
            break;
        case 5:
            glutKeyboardFunc(teclado_normal_fps);
            glutSpecialFunc(teclado_especial_fps);
            tipo_camera=2;
            break;
        case 6:
        	glEnable(GL_LIGHTING);
        	break;
        case 7:
        	glDisable(GL_LIGHTING);
        	break;
        default:
            break;
    }
    glutPostRedisplay();
}

int main(int argc, char *argv[]) {

	TiXmlDocument doc;
	TiXmlElement *root=NULL;
	TiXmlNode *light = NULL;

	if (doc.LoadFile(argv[1])) {
		root = doc.RootElement();
		scene = root->FirstChild("cena");
		
		// Initialization
		glutInit(&argc, argv);
		glutInitDisplayMode(GLUT_DEPTH|GLUT_DOUBLE|GLUT_RGBA);
		glutInitWindowPosition(100,100);
		glutInitWindowSize(800,800);
		glutCreateWindow("CG@DI-UM");
		glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
		
		if (scene) {

			// Regist functions
			glutDisplayFunc(renderScene);
			glutReshapeFunc(changeSize);
			glutIdleFunc(renderScene);


			if (tipo_camera == 0) {
				glutKeyboardFunc(teclado_normal_explorador);
				glutSpecialFunc(teclado_especial_explorador);
				glutMouseFunc(rato_explorador);
				glutMotionFunc(mov_rato_explorador);
				tipo_camera=1;
			}

			// Menu
			glutCreateMenu(front_menu);
		    glutAddMenuEntry("GL POINT",1);
		    glutAddMenuEntry("GL LINE",2);
		    glutAddMenuEntry("GL FILL",3);
		    glutAddMenuEntry("Modo Explorador",4);
		    glutAddMenuEntry("Modo FPS",5);
		    glutAddMenuEntry("Ligar Luz",6);
		    glutAddMenuEntry("Desligar Luz",7);


		    glutAttachMenu(GLUT_RIGHT_BUTTON);

		    glewInit();

            glEnableClientState(GL_VERTEX_ARRAY);
            glEnableClientState(GL_NORMAL_ARRAY);

			// OpenGL Settings
			glEnable(GL_DEPTH_TEST);
			glEnable(GL_CULL_FACE);

			if (light = (scene->FirstChild("luzes"))) {
		        glEnable(GL_LIGHTING);
	    	    loadLights(light);
			}
			
			loadXMLModels(scene);

			glutMainLoop();

		}
	}
	return 1;
}
