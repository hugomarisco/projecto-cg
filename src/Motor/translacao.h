#ifndef __TRANSLATION__
#define __TRANSLATION__

#include <iostream>
#include "tinyxml/tinyxml.h"
#include "points.h"

typedef struct sTranslations {
    float res[3];
    int point_count;
    Points *points;
    float period;
    float a;
    float lastTime;
    float pX;
    float pY;
    float pZ;
    struct sTranslations *next;
} Translations;

Translations* pushTranslation(Points *points, Translations *translations, int nPoints, float period, float x, float y, float z);
Translations* execTranslation(Translations *translation, long currentTime);

#endif