#include "motor.h"

Models modelList = NULL;

void loadXMLModels(TiXmlNode *scene) {

    TiXmlNode *child;
    TiXmlAttribute * attr;
    const char* tag;
    Vbo vbo;
    Points *points = NULL;

    int nPoints = -1, flag = 0;
    float x = 0.0, y = 0.0, z = 0.0, period = 0.0;

    for (child = scene->FirstChild(); child; child=child->NextSibling()) {
        tag = child->Value();
        if (strcmp(tag, "grupo") == 0) {
            loadXMLModels((child));
        } else if (strcmp(tag,"modelos") == 0) {
            loadXMLModels((child));
        } else {
            if (strcmp(tag, "modelo") == 0) {
                attr = child->ToElement()->FirstAttribute();
                if (strcmp(attr->Name(), "ficheiro") == 0) {
                    vbo = searchVBO(attr->Value(), modelList);
                    if (!vbo)
                        modelList = readVBO(attr->Value(), modelList);
                }
            } else if (strcmp(tag, "escala") == 0) {
                x = 1.0; y = 1.0; z = 1.0;
                for (attr = child->ToElement()->FirstAttribute(); attr; attr = attr->Next()) {
                    if (strcmp(attr->Name(), "x") == 0) x = atof(attr->Value());
                    else if (strcmp(attr->Name(), "y") == 0) y = atof(attr->Value());
                        else if (strcmp(attr->Name(), "z") == 0) z = atof(attr->Value());
                }
                scales = pushScale(x,y,z,scales);
            } else if (strcmp(tag, "rotacao") == 0) {
                x = 0.0; y = 0.0; z = 0.0; period = 0.0;
                for (attr = child->ToElement()->FirstAttribute(); attr; attr = attr->Next()) {
                    if (strcmp(attr->Name(), "eixoX") == 0) x = atof(attr->Value());
                        else if (strcmp(attr->Name(), "eixoY") == 0) y = atof(attr->Value());
                            else if (strcmp(attr->Name(), "eixoZ") == 0) z = atof(attr->Value());
                                else if (strcmp(attr->Name(), "tempo") == 0) period = atof(attr->Value());
                }
                rotations = pushRotation(period, x, y, z, rotations);
            } else if (strcmp(tag, "translacao") == 0) {
                x = 0.0; y = 0.0; z = 0.0;
                flag = 0; period = 0.0;
                for (attr = child->ToElement()->FirstAttribute(); attr; attr = attr->Next()) {
                    if (strcmp(attr->Name(), "tempo") == 0)
                        period = atof(attr->Value());
                    else if (strcmp(attr->Name(), "x") == 0) {
                        x = atof(attr->Value()); flag = 1;
                    } else if (strcmp(attr->Name(), "y") == 0) {
                        y = atof(attr->Value()); flag = 1;
                    } else if (strcmp(attr->Name(), "z") == 0) {
                        z = atof(attr->Value()); flag = 1;
                    }
                }
                if (flag == 1) period = 0.0;
                points = NULL; nPoints = -1;
                nPoints = readPoints(child, &points);
                translations = pushTranslation(points, translations, nPoints, period, x, y, z);
            }
        }
    }
}

void drawXMLModels(TiXmlNode *scene) {

    TiXmlNode *child;
    TiXmlAttribute * attr;
    const char* tag;
    Vbo vbo;

    for (child = scene->FirstChild(); child; child=child->NextSibling()) {
        tag = child->Value();
        if (strcmp(tag, "grupo")==0) {
            glPushMatrix();
            drawXMLModels((child));
            glPopMatrix();
        } else if (strcmp(tag,"modelos") == 0) {
            drawXMLModels((child));
        } else {
            if (strcmp(tag, "modelo") == 0) {
                attr = child->ToElement()->FirstAttribute();
                if (strcmp(attr->Name(), "ficheiro") == 0) {
                    vbo = searchVBO(attr->Value(), modelList);
                    if (vbo)
                        drawVBO(vbo);
                }
            } else if (strcmp(tag, "escala") == 0) {
                    currentScale = execScale(currentScale);
            } else if (strcmp(tag, "rotacao") == 0) {
                    currentRotation = execRotation(currentRotation, currentTime);
            } else if (strcmp(tag, "translacao") == 0) {
                    currentTranslation = execTranslation(currentTranslation, currentTime);
            }
        }
    }
}