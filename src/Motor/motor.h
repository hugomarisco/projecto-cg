#ifndef _MOTOR_H
#define _MOTOR_H
#define _USE_MATH_DEFINES

#include <math.h>
#include <stdio.h>
#include <string>
#include <iostream>

#include "vbo.h"
#include "tinyxml/tinyxml.h"
#include "escala.h"
#include "rotacao.h"
#include "translacao.h"
#include "points.h"

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

extern long currentTime;
extern Rotations *rotations, *currentRotation;
extern Scale scales, currentScale;
extern Translations *translations, *currentTranslation;

void loadXMLModels(TiXmlNode *scene);
void drawXMLModels(TiXmlNode *scene);

#endif
