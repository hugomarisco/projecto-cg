#include <stdlib.h>
#include "camera_FPS.h"

float angCamFPS_v = -0.2, angCamFPS_h = M_PI, px = 0, py = 2, pz = 5;

void modo_fps(){

    //Câmera em modo fps
	gluLookAt(px,py,pz,
	          px+sin(angCamFPS_h)*cos(angCamFPS_v), py+sin(angCamFPS_v), pz+cos(angCamFPS_h)*cos(angCamFPS_v),
              0.0f, 1.0f, 0.0f);


}

void teclado_especial_fps(int tecla, int x, int y) {
    switch (tecla) {
        case GLUT_KEY_UP:
            if(angCamFPS_v+0.05<M_PI_2)   //Para câmera não virar ao contrário
                angCamFPS_v+=0.05;
            break;
        case GLUT_KEY_DOWN:
           if(angCamFPS_v-0.05>-M_PI_2)  //Para câmera não virar ao contrário
                angCamFPS_v-=0.05;
            break;

        case GLUT_KEY_LEFT:
            angCamFPS_h+=0.05;
            break;
        case GLUT_KEY_RIGHT:
            angCamFPS_h-=0.05;
            break;

        default:
            break;
    }
    glutPostRedisplay();
}

void teclado_normal_fps(unsigned char tecla, int x, int y) {
    switch (tecla) {
        case 'd':
            px+=0.1*sin(angCamFPS_h - M_PI_2);
            pz+=0.1*cos(angCamFPS_h - M_PI_2);
            break;
        case 'a':
            px+=0.1*sin(angCamFPS_h + M_PI_2);
            pz+=0.1*cos(angCamFPS_h + M_PI_2);
            break;
        case 'w':
            px+=0.1*sin(angCamFPS_h);
            pz+=0.1*cos(angCamFPS_h);
            break;
        case 's':
            px-=0.1*sin(angCamFPS_h);
            pz-=0.1*cos(angCamFPS_h);
            break;
		case 'r':
			py += 0.3;
			break;
		case 'f':
			py -= 0.3;
			break;
		case 27:
			exit(0);


        default:
            break;
    }
    glutPostRedisplay();

}
