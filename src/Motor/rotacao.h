#ifndef __ROTACOES__
#define __ROTACOES__

#include <iostream>
#include <GL/glut.h>

typedef struct sRotations {
    int periodo;
    float eixoX;
    float eixoY;
    float eixoZ;
    struct sRotations *next;
} Rotations;

Rotations *pushRotation(float periodo, float eixoX, float eixoY, float eixoZ, Rotations *rotations);
Rotations *execRotation(Rotations *rot, long currentTime);

#endif