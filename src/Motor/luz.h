#ifndef __LUZ__
#define __LUZ__

#include <iostream>

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include "tinyxml/tinyxml.h"

typedef struct sluz{
    int luz;
    int tipo;
    float propriedade[4];
    struct sluz *next;
    
}*Luz, NLuz;

void loadLights(TiXmlNode* root);
void setLights();

#endif