#ifndef _CAMERAFPS_
#define _CAMERAFPS_

#include <math.h>

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

//Modo FPS
void modo_fps();
void teclado_especial_fps(int key, int x, int y);
void teclado_normal_fps(unsigned char key, int x, int y);

#endif
