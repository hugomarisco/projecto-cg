#include "circle.h"

void circle(float y, float radius, float slices, FILE *f) {

	float degree = ((2*M_PI)/slices), x_aux = 0.0, y_aux = 0.0;

	for (int i = 0; i <= slices; i++) {

		x_aux = y_aux;
		y_aux += degree;

		fprintf(f,"%f %f %f\n", 0.0, y, 0.0);
		fprintf(f,"%f %f %f\n", radius * sin(x_aux), y, radius * cos(x_aux));
		fprintf(f,"%f %f %f\n", radius * sin(y_aux), y, radius * cos(y_aux));

		fprintf(f,"%f %f %f\n", 0.0, y,  0.0);
		fprintf(f,"%f %f %f\n", radius * sin(y_aux), y, radius * cos(y_aux));
		fprintf(f,"%f %f %f\n", radius * sin(x_aux), y, radius * cos(x_aux));
	}
}

void circleOnlyBottom(float y, float radius, float slices, FILE *f) {
	float degree = ((2*M_PI)/slices), x_aux = 0.0, y_aux = 0.0;

	for (int i = 0; i <= slices; i++) {

		x_aux = y_aux;
		y_aux += degree;

		fprintf(f,"%f %f %f\n", 0.0, y,  0.0);
		fprintf(f,"%f %f %f\n", radius * sin(y_aux), y, radius * cos(y_aux));
		fprintf(f,"%f %f %f\n", radius * sin(x_aux), y, radius * cos(x_aux));
	}
}

void circleOnlyTop(float y, float radius, float slices, FILE *f) {
	float degree = ((2*M_PI)/slices), x_aux = 0.0, y_aux = 0.0;

	for (int i = 0; i <= slices; i++) {

		x_aux = y_aux;
		y_aux += degree;

		fprintf(f,"%f %f %f\n", 0.0, y, 0.0);
		fprintf(f,"%f %f %f\n", radius * sin(x_aux), y, radius * cos(x_aux));
		fprintf(f,"%f %f %f\n", radius * sin(y_aux), y, radius * cos(y_aux));
	}
}

void circleVBO(float y, float radius, float slices, FILE *f) {
	float degree = ((2*M_PI)/slices), x_aux = 0.0, y_aux = 0.0;

	int v = 0, i = 0;

	int points = (slices+1)*3;
	int indexes = (2*slices)*3;


	float *vertex = (float *) malloc (sizeof(float) * points);
	int *index = (int *) malloc (sizeof(int) * indexes);

	vertex[v++] = 0.0;
	vertex[v++] = y;
	vertex[v++] = 0.0;

	for (int j = 0; j < slices; j++) {

		vertex[v++] = radius * sin(y_aux);
		vertex[v++] = y;
		vertex[v++] = radius * cos(y_aux);

		index[i++] = 0;
		index[i++] = j+1;
		index[i++] = j+2;

		y_aux += degree;
	}

	index[i-1] = 1;

	for (int j = slices; j > 0; j--) {
		index[i++] = 0;
		index[i++] = j;
		index[i++] = j-1;
	}

	index[i-1] = slices;

	fprintf(f, "%d\n", points);
	for (i = 0; i < points; i+=3) {
		fprintf(f, "%f %f %f\n", vertex[i], vertex[i+1], vertex[i+2]);
	}

	fprintf(f, "%d\n", indexes);
	for (i = 0; i < indexes; i+=3) {
		fprintf(f, "%d %d %d\n", index[i], index[i+1], index[i+2]);
	}
}	

void circleOnlyBottomVBO(float y, float radius, float slices, FILE *f) {
	float degree = ((2*M_PI)/slices), x_aux = 0.0, y_aux = 0.0;

	int v = 0, i = 0;

	int points = (slices+1)*3;
	int indexes = (slices)*3;


	float *vertex = (float *) malloc (sizeof(float) * points);
	int *index = (int *) malloc (sizeof(int) * indexes);

	vertex[v++] = 0.0;
	vertex[v++] = y;
	vertex[v++] = 0.0;

	for (int j = 0; j < slices; j++) {

		vertex[v++] = radius * sin(y_aux);
		vertex[v++] = y;
		vertex[v++] = radius * cos(y_aux);

		y_aux += degree;
	}

	index[i-1] = 1;

	for (int j = slices; j > 0; j--) {
		index[i++] = 0;
		index[i++] = j;
		index[i++] = j-1;
	}

	index[i-1] = slices;

	fprintf(f, "%d\n", points);
	for (i = 0; i < points; i+=3) {
		fprintf(f, "%f %f %f\n", vertex[i], vertex[i+1], vertex[i+2]);
	}

	fprintf(f, "%d\n", indexes);
	for (i = 0; i < indexes; i+=3) {
		fprintf(f, "%d %d %d\n", index[i], index[i+1], index[i+2]);
	}
}

void circleOnlyTopVBO(float y, float radius, float slices, FILE *f) {
	float degree = ((2*M_PI)/slices), x_aux = 0.0, y_aux = 0.0;

	int v = 0, i = 0;

	int points = (slices+1)*3;
	int indexes = (slices)*3;


	float *vertex = (float *) malloc (sizeof(float) * points);
	int *index = (int *) malloc (sizeof(int) * indexes);

	vertex[v++] = 0.0;
	vertex[v++] = y;
	vertex[v++] = 0.0;

	for (int j = 0; j < slices; j++) {

		vertex[v++] = radius * sin(y_aux);
		vertex[v++] = y;
		vertex[v++] = radius * cos(y_aux);

		index[i++] = 0;
		index[i++] = j+1;
		index[i++] = j+2;

		y_aux += degree;
	}

	index[i-1] = 1;

	fprintf(f, "%d\n", points);
	for (i = 0; i < points; i+=3) {
		fprintf(f, "%f %f %f\n", vertex[i], vertex[i+1], vertex[i+2]);
	}

	fprintf(f, "%d\n", indexes);
	for (i = 0; i < indexes; i+=3) {
		fprintf(f, "%d %d %d\n", index[i], index[i+1], index[i+2]);
	}
}