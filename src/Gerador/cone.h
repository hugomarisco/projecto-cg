#ifndef _CONE_
#define _CONE_
#define _USE_MATH_DEFINES

#include <stdio.h>
#include <math.h>

void cone(float height, float radius, int slices, FILE *f);

#endif
