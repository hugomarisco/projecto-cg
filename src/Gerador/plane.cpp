#include "plane.h"

void plano( float x, float y, float z, float larg, float comp, float larg_slices, float comp_slices, int tipo,  FILE *f) {
 	float i, j, alpha = larg / larg_slices, beta = comp / comp_slices;

     printf("A \"gerar\" plano\n");

     printf("%d\n",tipo);

 	switch(tipo) {

 		case 1:
 			y -= larg / 2;
			z -= comp / 2;

			for (i = 0; i<larg; i += alpha){
				for (j = 0; j<comp; j += beta){

					fprintf(f,"%f %f %f\n", x, y + i, z + j);
					fprintf(f,"%f %f %f\n", x, y + i + alpha, z + j + beta);
					fprintf(f,"%f %f %f\n", x, y + i, z + j + beta);


					fprintf(f,"%f %f %f\n", x, y + i, z + j);
					fprintf(f,"%f %f %f\n", x, y + i + alpha, z + j);
					fprintf(f,"%f %f %f\n", x, y + i + alpha, z + j + beta);

				}
			}
			break;

		case 2:
			y -= larg / 2;
			z -= comp / 2;

			for (i = 0; i<larg; i += alpha){
				for (j = 0; j<comp; j += beta){

					fprintf(f,"%f %f %f\n", x, y + i, z + j);
					fprintf(f,"%f %f %f\n", x, y + i, z + j + beta);
					fprintf(f,"%f %f %f\n", x, y + i + alpha, z + j + beta);

					fprintf(f,"%f %f %f\n", x, y + i, z + j);
					fprintf(f,"%f %f %f\n", x, y + i + alpha, z + j + beta);
					fprintf(f,"%f %f %f\n", x, y + i + alpha, z + j);

				}
			}
			break;


		case 3:
			x -= larg / 2;
			z -= comp / 2;

			for (i = 0; i<larg; i += alpha){
				for (j = 0; j<comp; j += beta){

					fprintf(f,"%f %f %f\n", x + i, y, z + j);
					fprintf(f,"%f %f %f\n", x + i, y, z + j + beta);
					fprintf(f,"%f %f %f\n", x + i + alpha, y, z + j + beta);

					fprintf(f,"%f %f %f\n", x + i, y, z + j);
					fprintf(f,"%f %f %f\n", x + i + alpha, y, z + j + beta);
					fprintf(f,"%f %f %f\n", x + i + alpha, y, z + j);

				}
			}
			break;


		case 4:
			x -= larg / 2;
			z -= comp / 2;

			for (i = 0; i<larg; i += alpha){
				for (j = 0; j<comp; j += beta){

					fprintf(f,"%f %f %f\n", x + i, y, z + j);
					fprintf(f,"%f %f %f\n", x + i + alpha, y, z + j + beta);
					fprintf(f,"%f %f %f\n", x + i, y, z + j + beta);

					fprintf(f,"%f %f %f\n", x + i, y, z + j);
					fprintf(f,"%f %f %f\n", x + i + alpha, y, z + j);
					fprintf(f,"%f %f %f\n", x + i + alpha, y, z + j + beta);

				}
			}
			break;


		case 5:
			y -= larg / 2;
			x -= comp / 2;

			for (i = 0; i<larg; i += alpha){
				for (j = 0; j<comp; j += beta){

					fprintf(f,"%f %f %f\n", x + j, y + i, z);
					fprintf(f,"%f %f %f\n", x + j + beta, y + i, z);
					fprintf(f,"%f %f %f\n", x + j + beta, y + i + alpha, z);

					fprintf(f,"%f %f %f\n", x + j, y + i, z);
					fprintf(f,"%f %f %f\n", x + j + beta, y + i + alpha, z);
					fprintf(f,"%f %f %f\n", x + j, y + i + alpha, z);

				}
			}
			break;


		case 6:
			y -= larg / 2;
			x -= comp / 2;

			for (i = 0; i<larg; i += alpha){
				for (j = 0; j<comp; j += beta){

					fprintf(f,"%f %f %f\n", x + j, y + i, z);
					fprintf(f,"%f %f %f\n", x + j + beta, y + i + alpha, z);
					fprintf(f,"%f %f %f\n", x + j + beta, y + i, z);

					fprintf(f,"%f %f %f\n", x + j, y + i, z);
					fprintf(f,"%f %f %f\n", x + j, y + i + alpha, z);
					fprintf(f,"%f %f %f\n", x + j + beta, y + i + alpha, z);

				}
			}
			break;
 	}












}
