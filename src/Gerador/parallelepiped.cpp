#include "parallelepiped.h"

void paralelipipedo ( float x, float y, float z, float larg, float comp, float alt, float larg_slices, float comp_slices, float alt_slices, FILE *f){
    plano( x + (comp / 2), y, z, alt, larg, alt_slices, larg_slices, 1, f);
    plano( x - (comp / 2), y, z, alt, larg, alt_slices, larg_slices, 2, f);
    plano( x, y + (alt / 2), z, comp, larg, comp_slices, larg_slices, 3, f);
    plano( x, y - (alt / 2), z, comp, larg, comp_slices, larg_slices, 4, f);
    plano( x, y, z + (larg / 2), alt, comp, alt_slices, comp_slices, 5, f);
    plano( x, y, z - (larg / 2), alt, comp, alt_slices, comp_slices, 6, f);
}
