#include "sphere.h"

void esfera(float radius, int slices, int stacks, FILE *f) {

    float alpha = (float) ((2 * M_PI) / slices), beta = (float) (M_PI / stacks);

    float h = M_PI_2, h_aux = M_PI_2, r = 0.0, r_aux = 0.0;

    h += beta;

    for (int i = 0; i < slices; i++) {
        r_aux = r;
        r += alpha;


        fprintf(f,"%f %f %f\n", radius * cos(h) * sin(r_aux), sin(h), radius * cos(h) * cos(r_aux));
        fprintf(f,"%f %f %f\n", radius * cos(h) * sin(r), sin(h), radius * cos(h) * cos(r));
        fprintf(f,"%f %f %f\n", radius * cos(h_aux) * sin(r), sin(h_aux), radius * cos(h_aux) * cos(r));
    }

    for (int j = 1; j < stacks - 1; j++) {
        h_aux = h;
        h += beta;
        r = 0;

        for (int i = 0; i < slices; i++) {
            r_aux = r;
            r += alpha;

            fprintf(f,"%f %f %f\n", radius * cos(h_aux) * sin(r_aux), sin(h_aux), radius * cos(h_aux) * cos(r_aux));
            fprintf(f,"%f %f %f\n", radius * cos(h) * sin(r_aux), sin(h), radius * cos(h) * cos(r_aux));
            fprintf(f,"%f %f %f\n", radius * cos(h_aux) * sin(r), sin(h_aux), radius * cos(h_aux) * cos(r));

            fprintf(f,"%f %f %f\n", radius * cos(h) * sin(r_aux), sin(h), radius * cos(h) * cos(r_aux));
            fprintf(f,"%f %f %f\n", radius * cos(h) * sin(r), sin(h), radius * cos(h) * cos(r));
            fprintf(f,"%f %f %f\n", radius * cos(h_aux) * sin(r), sin(h_aux), radius * cos(h_aux) * cos(r));
        }
    }

    h_aux = h;
    h += beta;
    r = 0;

    for (int i = 0; i < slices; i++) {
        r_aux = r;
        r += alpha;

        fprintf(f,"%f %f %f\n", radius * cos(h_aux) * sin(r_aux), sin(h_aux), radius * cos(h_aux) * cos(r_aux));
        fprintf(f,"%f %f %f\n", radius * cos(h) * sin(r_aux), sin(h), radius * cos(h) * cos(r_aux));
        fprintf(f,"%f %f %f\n", radius * cos(h_aux) * sin(r), sin(h_aux), radius * cos(h_aux) * cos(r));

    }
}

void esferaVBO(float radius, int slices, int stacks, FILE *f) {
    float alpha = (float) ((2 * M_PI) / slices), beta = (float) (M_PI / stacks);

    float h = M_PI_2, h_aux = M_PI_2, r = 0.0, r_aux = 0.0;

    int v = 0, i = 0, n = 0, aux = 0;

    int points = (slices*(stacks-1)+2)*3;
    int indexes = (slices*(stacks-1)*2)*3;

    float *vertex = (float *) malloc (sizeof(float) * points);
    float *normal = (float *) malloc (sizeof(float) * points);
    int *index = (int *) malloc (sizeof(int) * indexes);

    h += beta;

    vertex[v++] = 0.0;
    vertex[v++] = radius;
    vertex[v++] = 0.0;

    normal[n++] = 0;
    normal[n++] = 1;
    normal[n++] = 0;

    for (int j = 0; j < slices; j++) {

        vertex[v++] = radius*cos(h_aux)*sin(r);
        vertex[v++] = sin(h);
        vertex[v++] = radius*cos(h_aux)*cos(r);

        normal[n++] = cos(h_aux)*sin(r);
        normal[n++] = sin(h);
        normal[n++] = cos(h_aux)*cos(r);

        index[i++] = 0;
        index[i++] = j+1;
        index[i++] = j+2;

        r += alpha;
    }

    index[i-1] = 1;
    
    for(int j = 1; j < stacks-1; j++){
        h_aux = h;
        h += beta;
        r = 0.0;

        for (int k = 0; k < stacks; k++) {
            aux = j*stacks+1;
            
            vertex[v++] = radius*cos(h)*sin(r);
            vertex[v++] = radius*sin(h);
            vertex[v++] = radius*cos(h)*cos(r);

            normal[n++] = cos(h)*sin(r);
            normal[n++] = sin(h);
            normal[n++] = cos(h)*cos(r);
        
            index[i++]=aux-stacks+k;
            index[i++]=aux+k ;
            index[i++]=aux-stacks+k+1;
            
            index[i++]=aux+k;
            index[i++]=aux+k+1;
            index[i++]=aux-stacks+k+1;
           
            r+=alpha;
        }

        index[i-4] = aux-stacks;
        index[i-2] = aux;
        index[i-1] = aux-stacks;
    }

    vertex[v++] = 0.0;
    vertex[v++] = -radius;
    vertex[v++] = 0.0;

    normal[n++]=0;
    normal[n++]=-1;
    normal[n++]=0;
    
    for (int j = 0; j < slices; j++) {
        
        index[i++] = aux+j;
        index[i++] = aux+slices;
        index[i++] = aux+j+1;
    }
    index[i-1] = aux;

    fprintf(f, "%d\n", points);
    for(i=0; i < points; i+=3)
        fprintf(f, "%f %f %f\n",vertex[i],vertex[i+1],vertex[i+2]);

    fprintf(f, "%d\n",indexes);
    for(i=0; i < indexes; i+=3)
        fprintf(f, "%d %d %d\n",index[i],index[i+1],index[i+2]); 

    for(i=0; i < points; i+=3)
        fprintf(f, "%f %f %f\n",normal[i],normal[i+1],normal[i+2]);

}