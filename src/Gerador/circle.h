#ifndef _CIRCLE_
#define _CIRCLE_
#define _USE_MATH_DEFINES

#include <stdio.h>
#include <math.h>
#include <stdlib.h>

void circle(float y, float radius, float slices, FILE *f);
void circleOnlyBottom(float y, float radius, float slices, FILE *f);
void circleOnlyTop(float y, float radius, float slices, FILE *f);
void circleVBO(float y, float radius, float slices, FILE *f);
void circleOnlyBottomVBO(float y, float radius, float slices, FILE *f);
void circleOnlyTopVBO(float y, float radius, float slices, FILE *f);

#endif
