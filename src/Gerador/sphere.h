#ifndef _SPHERE_
#define _SPHERE_
#define _USE_MATH_DEFINES

#include <stdio.h>
#include <math.h>
#include <stdlib.h>

void esfera(float radius, int slices, int stacks, FILE *f);
void esferaVBO(float radius, int slices, int stacks, FILE *f);

#endif
