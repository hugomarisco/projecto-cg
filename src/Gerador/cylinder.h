#ifndef _CYLINDER_
#define _CYLINDER_
#define _USE_MATH_DEFINES

#include <stdio.h>
#include <math.h>
#include "circle.h"

void cilindro(float height, float radius, float slices, FILE *f);

#endif
