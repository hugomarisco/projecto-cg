#include <string.h>
#include <errno.h>
#include <stdlib.h>

#include "circle.h"
#include "sphere.h"
#include "cone.h"
#include "plane.h"
#include "parallelepiped.h"

/*
Gerar ficheiros.3d que contêm os triangulos necessários para gerar:
	áreas planas	 ===> done
	paralelepípedos	 ===> done
	esferas			 ===> done
	cones   		 ===> done
	cilindro		 ===> almost done
*/

void circuloConeUtilizacao() {
	printf("\t<circulo|cone>    <altura> <raio> <fatias> [ficheiro.3d]\n");
}

void esferaUtilizacao() {
	printf("\t<esfera>          <raio> <fatias> <pilhas> [ficheiro.3d]\n");
}

void planoUtilizacao() {
	printf("\t<plano>           <x> <y> <z> <larg> <comp> <larg_slices> <comp_slices> <tipo> [ficheiro.3d]\n");
}

void paralelipipedoUtilizacao() {
	printf("\t<paralelipipedo>  <x> <y> <z> <larg> <comp> <alt> <larg_slices> <comp_slices> <alt_slices> [ficheiro.3d]\n");
}

int main(int argc, char *argv[]) {

	if (argc < 2) {
		printf("**ERRO** Necessário passar modelo a gerar!\n\n");
		printf("Utilização:\n");
		printf("\t<circulo|cone>    <altura> <raio> <fatias> [ficheiro.3d]\n");
		printf("\t<esfera>          <raio> <fatias> <pilhas> [ficheiro.3d]\n");
		printf("\t<plano>           <x> <y> <z> <larg> <comp> <larg_slices> <comp_slices> <tipo> [ficheiro.3d]\n");
		printf("\t<paralelipipedo>  <x> <y> <z> <larg> <comp> <alt> <larg_slices> <comp_slices> <alt_slices> [ficheiro.3d]\n");
		printf("\nCaso não seja passado um ficheiro para output, o default será modelo.3d\n\n");
		exit(0);
	}

	FILE *f = NULL;
	float radius = 0.0, slices = 0.0, stacks = 0.0, height = 0.0;
	float x = 0.0, y = 0.0, z = 0.0,
		  larg = 0.0, comp = 0.0, alt = 0.0,
		  larg_slices = 0.0, comp_slices = 0.0, alt_slices = 0.0;
	int tipo = 0;
	const char *filename = "modelo.3d";

	if (strcmp(argv[1],"circulo") == 0) {
 		if (argc >= 5) {
			sscanf(argv[2],"%f",&height);
			sscanf(argv[3],"%f",&radius);
			sscanf(argv[4],"%f",&slices);

			if(argc == 6) { filename = strdup(argv[5]); }
			f = fopen(filename, "w");
			if (f != NULL) {
				circleVBO(height,radius,slices,f);
				if (fclose(f) == 0) {
					printf("Good\n");
				}
				else
					printf("Erro %s\n", strerror(errno));
			}
			else { printf("Erro %s ao abrir o ficheiro %s\n", strerror(errno), filename); }
		}
		else {
			printf("Número de argumentos insuficientes\n");
			circuloConeUtilizacao();
		}
	}

	if (strcmp(argv[1], "cone") == 0) {
		if (argc >= 5) {
			sscanf(argv[2],"%f",&height);
			sscanf(argv[3],"%f",&radius);
			sscanf(argv[4],"%f",&slices);

			if(argc == 6) { filename = strdup(argv[5]); }
			f = fopen(filename, "w");
			if (f != NULL) {
				cone(height,radius,slices,f);
				fclose(f);
			} else { printf("Erro %s ao abrir o ficheiro %s\n", strerror(errno), filename); }
		} else {
			printf("Número de argumentos insuficientes\n");
			circuloConeUtilizacao();
		}
	}

	if (strcmp(argv[1], "esfera") == 0) {
		if (argc >= 5) {
			sscanf(argv[2],"%f",&radius);
			sscanf(argv[3],"%f",&slices);
			sscanf(argv[4],"%f",&stacks);

			if(argc == 6) { filename = strdup(argv[5]); }
			f = fopen(filename, "w");
			if (f != NULL) {
				esferaVBO(radius,slices,stacks,f);
				if (fclose(f) == 0) printf("Good\n");
				else printf("Erro %s\n", strerror(errno));
			} else { printf("Erro %s ao abrir o ficheiro %s\n", strerror(errno), filename); }
		} else {
			printf("Número de argumentos insuficientes\n");
			esferaUtilizacao();
		}
	}

	if (strcmp(argv[1],"plano") == 0) {
		if (argc >= 10) {
			sscanf(argv[2],"%f",&x);
			sscanf(argv[3],"%f",&y);
			sscanf(argv[4],"%f",&z);
			sscanf(argv[5],"%f",&larg);
			sscanf(argv[6],"%f",&comp);
			sscanf(argv[7],"%f",&larg_slices);
			sscanf(argv[8],"%f",&comp_slices);
			sscanf(argv[9],"%d",&tipo);

			if (argc == 11) { filename = strdup(argv[10]); }
			f = fopen(filename, "w");
			if (f != NULL) {
				plano(x,y,z,larg,comp,larg_slices,comp_slices,tipo,f);
				fclose(f);
			} else { printf("Erro %s ao abrir o ficheiro %s\n", strerror(errno), filename); }
		} else {
			printf("Número de argumentos insuficientes\n");
			planoUtilizacao();
		}
	}

	if (strcmp(argv[1],"paralelipipedo") == 0) {
		if (argc >= 11) {
			sscanf(argv[2],"%f",&x);
			sscanf(argv[3],"%f",&y);
			sscanf(argv[4],"%f",&z);
			sscanf(argv[5],"%f",&larg);
			sscanf(argv[6],"%f",&comp);
			sscanf(argv[7],"%f",&alt);
			sscanf(argv[8],"%f",&larg_slices);
			sscanf(argv[9],"%f",&comp_slices);
			sscanf(argv[10],"%f",&alt_slices);

			if (argc == 12) { filename = strdup(argv[11]); }
			f = fopen(filename, "w");
			if (f != NULL) {
				paralelipipedo(x,y,z,larg,comp,alt,larg_slices,comp_slices,alt,f);
				fclose(f);
			} else { printf("Erro %s ao abrir o ficheiro %s\n", strerror(errno), filename); }
		} else {
			printf("Número de argumentos insuficientes\n");
			paralelipipedoUtilizacao();
		}
	}

	return 0;
}
