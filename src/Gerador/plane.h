#ifndef _PLAN_
#define _PLAN_
#define _USE_MATH_DEFINES

#include <stdio.h>
#include <math.h>

void plano( float x, float y, float z, float larg, float comp, float larg_slices, float comp_slices, int tipo,  FILE *f);

#endif
