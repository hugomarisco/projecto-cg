#include "cone.h"
#include "circle.h"

void cone(float height, float radius, int slices, FILE *f) {

    float alpha = (float) ((2 * M_PI) / slices);

    float h = M_PI, h_aux = M_PI_2, r = 0.0, r_aux = 0.0;

    for (int i = 0; i < slices; i++) {
        r_aux = r;
        r += alpha;


        fprintf(f,"%f %f %f\n", radius * cos(h) * sin(r_aux), sin(h), radius * cos(h) * cos(r_aux));
        fprintf(f,"%f %f %f\n", radius * cos(h) * sin(r), sin(h), radius * cos(h) * cos(r));
        fprintf(f,"%f %f %f\n", radius * cos(h_aux) * sin(r), height * sin(h_aux), radius * cos(h_aux) * cos(r));
    }

    circleOnlyBottom(0,radius,slices,f);
}
